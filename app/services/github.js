(function () {

    "use strict";
    
    angular
        .module("app")
        .factory("github", ["$http", github]);
    
    function github ($http) {
        
        var url = "/app/MOCK_DATA.json"
        //var url = https://api.github.com/users/";
        
        var collaborators = [{ login: "collaborator 1" },
                             { login: "collaborator 2" }];

        var service =  {
            getUser: getUser,
            getRepos: getRepos,
            getRepoDetails: getRepoDetails
        };
        
        return service;
        
        // Public API
        
        function getUser (username) {
            return $http.get(url)
                .then(function (response) {
                    return filterUser(response.data, username);
                });
        }
        
        function getRepos (username) {
            return $http.get(url)
                .then(function (response) {
                    var user = filterUser(response.data, username);
                    return user.repos;
                });
        }
        
        function getRepoDetails (username, reponame) {
            // TODO: Build github api URL
            var repo;
            
            return $http.get(url) 
                .then(function(response) {
                    var user = filterUser(response.data, username);
                    repo = filterRepo(user.repos, reponame);
                    repo.username = user.username;
                    // Chain with collaborators retrieval
                    return $http.get(url);
                })
                .then(function (response) {
                    // response.data should contain collaborators info
                    repo.collaborators = collaborators;
                    return repo;
                });
            
        }
        
        // Private methods
        
        function filterUser (data, username) {
            return data.filter(function (item) {
                return item.username === username;
            })[0];
        }
        
        function filterRepo (repos, reponame) {
            return repos.filter(function (item) {
                return item.name === reponame;
            })[0];
        }
    }

}());