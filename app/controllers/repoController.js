(function () {

    "use strict";
    
    angular
        .module("app")
        .controller("RepoController", ["$scope", "$routeParams", "github", repoController]);
    
    function repoController ($scope, $routeParams, github) {

        $scope.username = $routeParams.username;
        $scope.reponame = $routeParams.reponame;
    
        loadRepoInfo();
        
        function onRepoDetails (response) {
            $scope.repo = response;
        }
        
        function onError (reason) {
        }
        
        function loadRepoInfo () {
            github.getRepoDetails($scope.username, $scope.reponame)
                .then(onRepoDetails)
                .catch(onError);
        }
    }

}());

