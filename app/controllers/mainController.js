(function () {

    "use strict";
    
    angular
        .module("app")
        .controller("MainController", ["$scope", "$interval", "$location", mainController]);
    
    function mainController ($scope, $interval, $location) {
        
        $scope.username = 'pwood0';
        $scope.search = search;
        $scope.countdown = 5;
        
        function search () {
            if (countdownInterval) {
                $interval.cancel(countdownInterval);
                $scope.countdown = null;
            }
            //$location.path("/user/" + $scope.username);
        }
        
        function decrementCountdown () {
            $scope.countdown--;
            if ($scope.countdown === 0) {
                search();
            }
        }
        
        var countdownInterval = null;
        function startCountdown () {
            countdownInterval = $interval(decrementCountdown, 1000, $scope.countdown);
        }
        
        startCountdown();
        
    }
    
    
}());