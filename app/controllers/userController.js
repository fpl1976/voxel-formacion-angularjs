(function () {

    "use strict";
    
    angular
        .module("app")
        .controller("UserController", ["$scope", "$routeParams", "github", userController]);
    
    function userController ($scope, $routeParams, github) {
        
        $scope.username;
        $scope.user;
        $scope.repos = [];
        $scope.error = null;
        $scope.repoSortOrder = "-stargazers_count";
        
        loadGithubUser();
        
        function onUserComplete (user) {
            $scope.user = user;
            $scope.gravatarUrl = "http://vignette2.wikia.nocookie.net/mugen/images/c/ca/Thor-AOU-Render.png/revision/latest?cb=20150614022944";
            
            github.getRepos($scope.username)
                .then(onRepos)
                .catch(onError);
        }
        
        function onRepos (repos) {
            $scope.repos = repos;
        }
        
        function onError (reason) {
            $scope.error = "Error loading";
        }
        
        function loadGithubUser () {
            $scope.username = $routeParams.username;
            return github.getUser($scope.username)
                .then(onUserComplete)
                .catch(onError);
        }
        
    }
    
    
}());