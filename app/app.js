(function() {

    "use strict";
    
    angular
        .module("app", ["ngRoute"])
        .config(["$routeProvider", configApp]);
    
    function configApp ($routeProvider) {
        
        $routeProvider
            .when("/main", {
                templateUrl: "app/templates/main.html",
                controller: "MainController"
            })
            .when("/user/:username", {
                templateUrl: "app/templates/userDetails.html",
                controller: "UserController"
            })
            .when("/repos/:username/:reponame", {
                templateUrl: "app/templates/repo.html",
                controller: "RepoController"
            })
            .otherwise("/main");
    }
                 
}());

